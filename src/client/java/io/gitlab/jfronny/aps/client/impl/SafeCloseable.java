package io.gitlab.jfronny.aps.client.impl;

public interface SafeCloseable extends AutoCloseable {
    @Override
    void close();
}
