package io.gitlab.jfronny.aps.client.impl;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ResourcePackOrganizerLockState {
    private final Lock resourcesLock = new ReentrantLock();

    public SafeCloseable lockResources() {
        resourcesLock.lock();
        return resourcesLock::unlock;
    }

    private boolean isRunning = false;
    private boolean isScheduled = false;
    private final Lock scanStateLock = new ReentrantLock();

    public ScanFinishedResponse emitScanFinished() {
        scanStateLock.lock();
        try {
            if (isScheduled) {
                isScheduled = false;
                return new ScanFinishedResponse(true);
            } else {
                isRunning = false;
                return new ScanFinishedResponse(false);
            }
        } finally {
            scanStateLock.unlock();
        }
    }

    public RequestScanResponse requestScan() {
        scanStateLock.lock();
        try {
            if (isRunning) {
                isScheduled = true;
                return new RequestScanResponse(false);
            } else {
                isRunning = true;
                return new RequestScanResponse(true);
            }
        } finally {
            scanStateLock.unlock();
        }
    }

    public record ScanFinishedResponse(boolean shouldContinue) {}
    public record RequestScanResponse(boolean shouldStart) {}
}
