package io.gitlab.jfronny.aps.testmod.mixin;

import io.gitlab.jfronny.libjf.LibJf;
import net.minecraft.resource.ResourcePackManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ResourcePackManager.class)
public class ResourcePackManagerMixin {
    @Inject(at = @At("TAIL"), method = "scanPacks()V")
    private void scanPacks(CallbackInfo info) throws InterruptedException {
        Thread.sleep(100);
        LibJf.LOGGER.info("Scanned Packs");
    }
}
