package io.gitlab.jfronny.aps;

import java.util.concurrent.Future;

public interface AsyncResourcePackManager {
    default Future<Void> scanPacksAsync(Runnable callback) {
        throw new IllegalStateException("Async pack scan not injected");
    }
}
