package io.gitlab.jfronny.aps.mixin;

import io.gitlab.jfronny.aps.AsyncResourcePackManager;
import io.gitlab.jfronny.aps.impl.VoidFuture;
import net.minecraft.resource.ResourcePackManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

@Mixin(ResourcePackManager.class)
public abstract class ResourcePackManagerMixin implements AsyncResourcePackManager {
    @Unique
    private static final ForkJoinPool APS$POOL = ForkJoinPool.commonPool();
    @Shadow
    public abstract void scanPacks();

    @Override
    public Future<Void> scanPacksAsync(Runnable callback) {
        return new VoidFuture(APS$POOL.submit(() -> {
            scanPacks();
            callback.run();
        }));
    }
}
