plugins {
    id("jfmod") version "1.6-SNAPSHOT"
}

allprojects { group = "io.gitlab.jfronny" }
base.archivesName = "async-pack-scan"

jfMod {
    minecraftVersion = "1.21.4"
    yarn("build.1")
    loaderVersion = "0.16.9"
    libJfVersion = "3.18.3"
    fabricApiVersion = "0.110.5+1.21.4"

    modrinth {
        projectId = "async-pack-scan"
        requiredDependencies.add("libjf")
    }

    curseforge {
        projectId = "881560"
        requiredDependencies.add("libjf")
    }
}

loom {
    accessWidenerPath.set(file("src/client/resources/async-pack-scan.accesswidener"))
}

dependencies {
    include(modImplementation("net.fabricmc.fabric-api:fabric-resource-loader-v0")!!)
    modImplementation("io.gitlab.jfronny.libjf:libjf-base")

    // For testing in dev environment
    modLocalRuntime("net.fabricmc.fabric-api:fabric-api")
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil")
    modLocalRuntime("com.terraformersmc:modmenu:12.0.0-beta.1")
}
