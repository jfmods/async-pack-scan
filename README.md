## Note
This mod no longer seems to be required as of MC 1.20.5.
Nonetheless, interactions between mods might still cause an issue, so I will keep updating it for the time being.
If you experience no lag spikes, you can safely remove this mod.

## Original Description
In vanilla minecraft, the list of resource packs is refreshed by scanning all possible sources synchronously.

Generally, this is not a problem, but in the resource pack organizer screen, this scan is performed whenever it is resized and every twenty ticks.

If another mod (such as respackopts) hooks into the pack scan and increases its duration even slightly, this leads to major lag spikes and makes the screen near-unusable when using more than a few packs.

This mod fixes that issue by moving this computation to another thread and scheduling a scan task on those events instead, drastically improving perceived performance.

Since the vanilla code is designed for synchronous execution, this can cause issues like crashes in edge cases, but worked well enough in my testing.
